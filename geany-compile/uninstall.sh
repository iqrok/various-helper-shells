#!/bin/sh

INITIAL_DIR="$(pwd)"
DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"

GEANY_DIR="${DIR}/geany"
GEANY_PLUGINS_DIR="${DIR}/geany-plugins"

cd "${GEANY_PLUGINS_DIR}" && sudo make uninstall-recursive
cd "${GEANY_DIR}" && sudo make uninstall-recursive
