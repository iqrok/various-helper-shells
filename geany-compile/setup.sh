#!/bin/sh

INITIAL_DIR="$(pwd)"
DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"

# ================================ geany =======================================

# install required packages
sudo apt install -y \
	autopoint automake autoconf libtool libgtk-3-dev intltool docutils

# clone geany repo
GEANY_DIR="${DIR}/geany"

if [ ! -d "${GEANY_DIR}" ]; then
	git clone https://github.com/geany/geany.git "${GEANY_DIR}"
else
	cd "${GEANY_DIR}" && git pull
fi

# compile and install geany
cd "${GEANY_DIR}" \
	&& ./autogen.sh \
	&& make -j$(nproc) \
	&& sudo make install

cd "${INITIAL_DIR}"

# ============================ geany-plugins ===================================

# install required packages
sudo apt install -y \
	libvte-2.91-dev libctpl-dev liblua5.1-0-dev libgpgme-dev libsoup-3.0-dev \
	libgit2-dev libwebkit2gtk-4.1-dev libxml2-dev libenchant-2-dev

# clone geany-plugins repo
GEANY_PLUGINS_DIR="${DIR}/geany-plugins"

if [ ! -d "${GEANY_PLUGINS_DIR}" ]; then
	git clone https://github.com/geany/geany-plugins.git "${GEANY_PLUGINS_DIR}"
else
	cd "${GEANY_PLUGINS_DIR}" && git pull
fi

# compile and install geany-plugins
cd "${GEANY_PLUGINS_DIR}" \
	&& ./autogen.sh \
	&& make -j$(nproc) \
	&& sudo make install

cd "${INITIAL_DIR}"
