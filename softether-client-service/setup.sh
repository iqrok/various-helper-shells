#!/bin/sh

if [ "$(id -u)" != "0" ]; then
	echo "Please run as root!"
	exit 1
fi

DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"

SOFTETHER_VPNCLIENT="$(command -v vpnclient)"
SOFTETHER_VPNCMD="$(command -v vpncmd)"

TMPL_VPN_IP_ALLOCATE=
TMPL_VPN_IP_DEALLOCATE=

process_vpn_file()
{
	VPN_ACCOUNT_FILE="${1}"
	PVF_COUNTER="${2}"

	if [ -z "${VPN_ACCOUNT_FILE}" ]; then
		echo "No account imported..."
		exit 0
	fi

	echo "===== ${VPN_ACCOUNT_FILE} ====="

	VPN_ACCOUNT_NAME="$(grep -Po 'AccountName[\s]+[a-zA-Z0-9_]+' "${VPN_ACCOUNT_FILE}" | awk '{ print $2 }')"
	VPN_NIC_NAME="$(grep -Po 'DeviceName[\s]+[a-zA-Z0-9_]+' "${VPN_ACCOUNT_FILE}" | awk '{ print $2 }')"
	VPN_IFACE="vpn_${VPN_NIC_NAME}"
	VPN_IP=

	while [ -z "${VPN_IP}" ]; do
		printf "IP and mask for '%s' (ex. 172.16.0.16/22): " "${VPN_IFACE}"
		read -r INPUT_IP

		TO_UPPER="$(echo "${INPUT_IP}" | tr '[:upper:]' '[:lower:]')"

		if [ "${TO_UPPER}" = "DHCP" ]; then
			VPN_IP="${TO_UPPER}"
			break;
		fi

		VPN_IP="$(echo "${INPUT_IP}" | grep -E "[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\/[0-9]{1,2}")"
		if [ -z "${VPN_IP}" ]; then
			echo "Invalid IP Address '${INPUT_IP}'!"
		fi
	done

	if [ "${PVF_COUNTER}" != "0" ]; then
		TMPL_VPN_IP_ALLOCATE="${TMPL_VPN_IP_ALLOCATE}\n\t\t"
		TMPL_VPN_IP_DEALLOCATE="${TMPL_VPN_IP_DEALLOCATE}\n\t\t"
	fi

	if [ "${VPN_IP}" = "DHCP" ]; then
		TMPL_VPN_IP_ALLOCATE="${TMPL_VPN_IP_ALLOCATE}dhclient ${VPN_IFACE} &"
		TMPL_VPN_IP_DEALLOCATE="${TMPL_VPN_IP_DEALLOCATE}dhclient -r ${VPN_IFACE}"
	else
		TMPL_VPN_IP_ALLOCATE="${TMPL_VPN_IP_ALLOCATE}ip a add ${VPN_IP} dev ${VPN_IFACE}"
		TMPL_VPN_IP_DEALLOCATE="${TMPL_VPN_IP_DEALLOCATE}ip a del ${VPN_IP} dev ${VPN_IFACE}"
	fi

	# create NIC
	"${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD NicCreate "${VPN_NIC_NAME}"

	# Import account
	printf "%s\n" "${VPN_ACCOUNT_FILE}" \
		| "${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD AccountImport

	# set account nic
	printf "%s\n" "${VPN_NIC_NAME}" \
		| "${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD AccountNicSet "${VPN_ACCOUNT_NAME}"

	# auto start account
	"${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD AccountStartupSet "${VPN_ACCOUNT_NAME}"

	# connect account to server
	"${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD AccountConnect "${VPN_ACCOUNT_NAME}"

	echo "acc: ${VPN_ACCOUNT_NAME}"
	echo "nic: ${VPN_NIC_NAME}"
	echo "ifc: ${VPN_IFACE}"
	echo "IP : ${VPN_IP}"
	echo ""
}

if [ -z "${SOFTETHER_VPNCLIENT}" ]; then
	apt install -y softether-vpnclient softether-vpncmd softether-common

	SOFTETHER_VPNCLIENT="$(command -v vpnclient)"
	SOFTETHER_VPNCMD="$(command -v vpncmd)"
fi

# sofether vpn configs
"${SOFTETHER_VPNCLIENT}" start

ARGS_COUNTER=0
while [ ${#} -gt 0 ]; do
	process_vpn_file "${1}" "${ARGS_COUNTER}"
	ARGS_COUNTER=$(( ARGS_COUNTER + 1 ))
	shift
done

"${SOFTETHER_VPNCLIENT}" stop

# vpnclient service
SRC_SHELL_FILE="${DIR}/softether-service.sh"
DST_SHELL_FILE="/usr/local/bin/softether-service.sh"

SRC_SERVICE_FILE="${DIR}/vpnclient.service"
DST_SERVICE_FILE="/etc/systemd/system/vpnclient.service"

sed "s,__VPN_IP_ALLOCATE__,${TMPL_VPN_IP_ALLOCATE},g" "${SRC_SHELL_FILE}" \
	| sed "s,__VPN_IP_DEALLOCATE__,${TMPL_VPN_IP_DEALLOCATE},g" \
	| tee "${DST_SHELL_FILE}" > /dev/null
chmod a+x "${DST_SHELL_FILE}"

sed "s,__SOFTETHER_SHELL__,${DST_SHELL_FILE},g" "${SRC_SERVICE_FILE}" \
	| tee "${DST_SERVICE_FILE}" > /dev/null

systemctl daemon-reload
systemctl enable vpnclient.service
systemctl start vpnclient.service
