#!/bin/sh

if [ "$(id -u)" != "0" ]; then
	echo "Please run as root!"
	exit 1
fi

SOFTETHER_VPNCLIENT="$(command -v vpnclient)"
SOFTETHER_VPNCMD="$(command -v vpncmd)"

delete_account()
{
	FD_ACCOUNT_NAME="${1}"

	# remove auto start account
	#~ printf "%s\n" "${FD_ACCOUNT_NAME}" \
		#~ | "${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD AccountStartupRemove

	# disconnect account
	#~ printf "%s\n" "${FD_ACCOUNT_NAME}" \
		#~ | "${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD AccountDisconnect

	# delete account
	#~ printf "%s\n" "${FD_ACCOUNT_NAME}" \
		#~ | "${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD AccountDelete
}

delete_nic()
{
	FD_NIC_NAME="${1}"

	# delete NIC
	#~ printf "%s\n" "${FD_NIC_NAME}" \
		#~ | "${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD NicDelete
}

if [ -z "${SOFTETHER_VPNCMD}" ]; then
	echo "SoftEther vpncmd is not installed!"
	exit 2
fi

SERVICE_NAME="vpnclient.service"
SERVICE_FILE="/etc/systemd/system/${SERVICE_NAME}"

DST_SHELL_FILE="/usr/local/bin/softether-service.sh"

# remove service
#~ systemctl stop "${SERVICE_NAME}"
#~ systemctl disable "${SERVICE_NAME}"

#~ rm "${SERVICE_FILE}" "${DST_SHELL_FILE}"
#~ systemctl daemon-reload

# restart vpnclient
#~ "${SOFTETHER_VPNCLIENT}" start

# Delete Account(s)
ACCOUNT_LIST="$( "${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD AccountList \
	| grep "VPN Connection Setting Name" \
	| awk -F'|' '{ print $2 }' )"

echo "${ACCOUNT_LIST}" | while read -r ACCOUNT_NAME; do
	delete_account "${ACCOUNT_NAME}"
done

# Delete NIC(s)
NIC_LIST="$( "${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD NicList \
	| grep "Virtual Network Adapter Name" \
	| awk -F'|' '{ print $2 }' )"

echo "${NIC_LIST}" | while read -r NIC_NAME; do
	delete_nic "${NIC_NAME}"
done

# stop vpnclient
#~ "${SOFTETHER_VPNCLIENT}" stop

# remove package
#~ apt remove --purge -y softether-vpnclient softether-vpncmd softether-common
