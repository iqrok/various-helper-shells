#!/bin/sh

SOFTETHER_VPNCLIENT="$(command -v vpnclient)"
SOFTETHER_VPNCMD="$(command -v vpncmd)"

case "${1}" in
	"start")
		"${SOFTETHER_VPNCLIENT}" start
		sleep 0.5
		__VPN_IP_ALLOCATE__
		;;

	"stop")
		"${SOFTETHER_VPNCLIENT}" stop
		sleep 0.5
		__VPN_IP_DEALLOCATE__
		;;

	"check")
		CON_STATUS="$("${SOFTETHER_VPNCMD}" localhost /CLIENT /CMD accountlist \
			| grep Status | grep -v Connected)"

		if [ -n "${CON_STATUS}" ]; then
			systemctl restart vpnclient.service
		fi
		;;


	*)
		;;

esac
