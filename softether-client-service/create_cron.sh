#!/bin/sh

if [ "$(id -u)" != "0" ]; then
	echo "Please run as root!"
	exit 1
fi

DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"

SOFTETHER_VPNCLIENT="$(command -v vpnclient)"
SOFTETHER_VPNCMD="$(command -v vpncmd)"

CRON_HOURLY_DIR="/etc/cron.hourly"
SHELL_VPN_RESTART_PATH="${CRON_HOURLY_DIR}/softether-restart"

# use cron.hourly instead crontab -e. If it doesn't exist, I give no shit
if [ ! -d "${CRON_HOURLY_DIR}" ]; then
	echo "Error: '${CRON_HOURLY_DIR}' doesn't exist!"
	exit 2
fi

if [ -z "${SOFTETHER_VPNCMD}" ]; then
	if [ -z "${1}" ]; then
		echo "Error: vpncmd is no in the path! please provide vpncmd path in arg."
		exit 1
	fi

	SOFTETHER_VPNCMD="${1}"
fi

create_restart_vpn_shell()
{
	ACCOUNT_NAME="${1}"
	PING_IP_TARGET="${2}"

	if [ ! -f "${SHELL_VPN_RESTART_PATH}" ]; then
		printf '#!/bin/sh\n\n' | tee "${SHELL_VPN_RESTART_PATH}" > /dev/null
		chmod a+x "${SHELL_VPN_RESTART_PATH}"
	fi

	CMD_PING="ping -c1 -w5 ${PING_IP_TARGET} > /dev/null"
	CMD_DISCONNECT="${SOFTETHER_VPNCMD} /CLIENT 127.0.0.1 /CMD AccountDisconnect ${ACCOUNT_NAME}"
	CMD_CONNECT="${SOFTETHER_VPNCMD} /CLIENT 127.0.0.1 /CMD AccountConnect ${ACCOUNT_NAME}"

	# tldr; if ping exit code isn't 0, restart connection via vpncmd
	printf 'if ! %s; then %s; %s; fi\nsleep 1\n' \
			"${CMD_PING}" \
			"${CMD_DISCONNECT}" \
			"${CMD_CONNECT}" \
		| tee -a "${SHELL_VPN_RESTART_PATH}" > /dev/null
}

# no idea how to calc ip range on native shell, use ipcalc instead
IPCALC="$(command -v ipcalc)"
if [ -z "${IPCALC}" ]; then
	apt install -y ipcalc
	IPCALC="$(command -v ipcalc)"
fi

# delete existing shell first
if [ -f "${SHELL_VPN_RESTART_PATH}" ]; then
	rm "${SHELL_VPN_RESTART_PATH}"
fi

# iterate through accounts to create shell file
"${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD AccountList \
	| grep 'VPN Connection Setting Name' \
	| sed -r 's,VPN Connection Setting Name.*?\|,,g' \
	| while read -r ACCOUNT_NAME; do

	ACCOUNT_DETAIL="$("${SOFTETHER_VPNCMD}" /CLIENT 127.0.0.1 /CMD AccountGet "${ACCOUNT_NAME}")"
	NIC_NAME="$(echo "${ACCOUNT_DETAIL}" \
		| grep 'Device Name Used for Connection' \
		| sed -r 's,Device Name Used for Connection.*?\|,,g')"
	IP_IFACE="vpn_$(echo "${NIC_NAME}" | tr [:upper:] [:lower:])"
	IP_RANGE="$(ip -br -4 a show dev ${IP_IFACE} | awk '{print $3}')"

	# use HostMin as ping target IP
	IP_HOST_MIN="$("${IPCALC}" -bn "${IP_RANGE}" \
		| grep 'HostMin' \
		| awk '{print $2}')"

	echo "${NIC_NAME} => ${IP_RANGE} to ${IP_HOST_MIN}"
	create_restart_vpn_shell "${ACCOUNT_NAME}" "${IP_HOST_MIN}"
done
