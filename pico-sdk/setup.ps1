param (
	$ToolchainInstall = 1,
	$ToolchainVersion = "13.2.rel1",

	$CmakeInstall = 1,
	$CmakeVersion = "3.30.3",

	$OpenocdInstall = 1,
	$OpenocdVersion = "0.12.0",

	$NinjaInstall = 1,
	$NinjaVersion = "1.12.1",

	$FreeRTOSInstall = 1,
	$FreeRTOSVersion = "202406-LTS",

	$PicoSDKInstall = 1,
	$PicoSDKVersion = "2.0.0",

	$SetupTitle = "PICO-SDK setup",
	$SetupVersion = "0.2.0"
)

$TARGET_DIR="$PSScriptRoot\pico"

$DOWNLOAD_DIR="$PSScriptRoot\pico\download"
$RES_DIR="$PSScriptRoot\pico\res"
$TMP_DIR="$PSScriptRoot\pico\tmp"

New-Item -ItemType Directory -Force -Path "$TARGET_DIR" | out-null
New-Item -ItemType Directory -Force -Path "$DOWNLOAD_DIR" | out-null
New-Item -ItemType Directory -Force -Path "$TMP_DIR" | out-null
New-Item -ItemType Directory -Force -Path "$RES_DIR" | out-null

# ========================= Setup arm-gnu-toolchain ============================
function setup_arm_gnu_toolchain()
{
	$PACKAGE_NAME="arm-gnu-toolchain"
	$PACKAGE_VERSION=$ToolchainVersion
	$PACKAGE_EXTENSION="zip"
	$PACKAGE_URL="https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu/$PACKAGE_VERSION/binrel/$PACKAGE_NAME-$PACKAGE_VERSION-mingw-w64-i686-arm-none-eabi.$PACKAGE_EXTENSION"
	$PACKAGE_DL_PATH="$DOWNLOAD_DIR\$PACKAGE_NAME-$PACKAGE_VERSION.$PACKAGE_EXTENSION"
	$PACKAGE_SECTION_PATH="$RES_DIR\$PACKAGE_NAME"
	$PACKAGE_RES_PATH="$PACKAGE_SECTION_PATH\$PACKAGE_VERSION"
	$PACKAGE_BIN_PATH="$PACKAGE_RES_PATH\bin"

	New-Item -ItemType Directory -Force -Path "$TMP_DIR" | out-null

	if (!(Test-Path $PACKAGE_RES_PATH)) {
		if (!(Test-Path $PACKAGE_DL_PATH)) {
			Write-Host "Downloading $PACKAGE_NAME from '$PACKAGE_URL'..."
			Invoke-WebRequest $PACKAGE_URL -OutFile $PACKAGE_DL_PATH
			if(!($?)) {
				Write-Host "[Failed] Download $PACKAGE_NAME"
				return $null
			}
		}

		$PACKAGE_UNZIPPED=(Get-ChildItem "$TMP_DIR" -Filter $PACKAGE_NAME-$PACKAGE_VERSION* -Name)

		if ($PACKAGE_UNZIPPED) {
			$PACKAGE_TMP_PATH=-join("$TMP_DIR\", "$PACKAGE_UNZIPPED")
		} else {
			Write-Host "Unzip '$PACKAGE_DL_PATH'..."
			Expand-Archive "$PACKAGE_DL_PATH" -DestinationPath "$TMP_DIR"
			$PACKAGE_TMP_PATH=-join("$TMP_DIR\", (Get-ChildItem "$TMP_DIR" -Filter $PACKAGE_NAME-$PACKAGE_VERSION* -Name))
		}

		Write-Host "Move '$PACKAGE_TMP_PATH' to '$PACKAGE_RES_PATH'"
		New-Item -ItemType Directory -Force -Path "$PACKAGE_SECTION_PATH" | out-null
		mv -Force "$PACKAGE_TMP_PATH" "$PACKAGE_RES_PATH"
	}

	Write-Host "Installed '$PACKAGE_NAME-$PACKAGE_VERSION' at '$PACKAGE_RES_PATH'"

	$global:ENV_PATH_STR=-join($global:ENV_PATH_STR, ";$PACKAGE_BIN_PATH")
}

# ========================= Setup openocd ============================
function setup_openocd()
{
	$PACKAGE_NAME="openocd"
	$PACKAGE_VERSION=$OpenocdVersion
	$PACKAGE_EXTENSION="tar.gz"
	$PACKAGE_URL="https://github.com/openocd-org/openocd/releases/download/v$PACKAGE_VERSION/openocd-v$PACKAGE_VERSION-i686-w64-mingw32.$PACKAGE_EXTENSION"
	$PACKAGE_DL_PATH="$DOWNLOAD_DIR\$PACKAGE_NAME-$PACKAGE_VERSION.$PACKAGE_EXTENSION"
	$PACKAGE_SECTION_PATH="$RES_DIR\$PACKAGE_NAME"
	$PACKAGE_RES_PATH="$PACKAGE_SECTION_PATH\$PACKAGE_VERSION"
	$PACKAGE_BIN_PATH="$PACKAGE_RES_PATH\bin"

	New-Item -ItemType Directory -Force -Path "$TMP_DIR" | out-null

	if (!(Test-Path $PACKAGE_RES_PATH)) {
		if (!(Test-Path $PACKAGE_DL_PATH)) {
			Write-Host "Downloading $PACKAGE_NAME from '$PACKAGE_URL'..."
			Invoke-WebRequest $PACKAGE_URL -OutFile $PACKAGE_DL_PATH
			if(!($?)) {
				Write-Host "[Failed] Download $PACKAGE_NAME"
				return $null
			}
		}

		$PACKAGE_UNZIPPED=(Get-ChildItem "$TMP_DIR" -Filter $PACKAGE_NAME-$PACKAGE_VERSION* -Name)

		if ($PACKAGE_UNZIPPED) {
			$PACKAGE_TMP_PATH=-join("$TMP_DIR\", "$PACKAGE_UNZIPPED")
		} else {
			Write-Host "Unzip '$PACKAGE_DL_PATH'..."
			tar -xf "$PACKAGE_DL_PATH" -C "$TMP_DIR"
			$PACKAGE_TMP_PATH=-join("$TMP_DIR\", (Get-ChildItem "$TMP_DIR" -Filter $PACKAGE_NAME-$PACKAGE_VERSION* -Name))
		}

		Write-Host "Move '$PACKAGE_TMP_PATH' to '$PACKAGE_RES_PATH'"
		New-Item -ItemType Directory -Force -Path "$PACKAGE_SECTION_PATH" | out-null
		mv -Force "$PACKAGE_TMP_PATH" "$PACKAGE_RES_PATH"
	}

	Write-Host "Installed '$PACKAGE_NAME-$PACKAGE_VERSION' at '$PACKAGE_RES_PATH'"

	$global:ENV_PATH_STR=-join($global:ENV_PATH_STR, ";$PACKAGE_BIN_PATH")
}

# ========================= Setup cmake ============================
function setup_cmake()
{
	$PACKAGE_NAME="cmake"
	$PACKAGE_VERSION=$CmakeVersion
	$PACKAGE_EXTENSION="zip"
	$PACKAGE_URL="https://github.com/Kitware/CMake/releases/download/v$PACKAGE_VERSION/cmake-$PACKAGE_VERSION-windows-x86_64.$PACKAGE_EXTENSION"

	$PACKAGE_DL_PATH="$DOWNLOAD_DIR\$PACKAGE_NAME-$PACKAGE_VERSION.$PACKAGE_EXTENSION"
	$PACKAGE_SECTION_PATH="$RES_DIR\$PACKAGE_NAME"
	$PACKAGE_RES_PATH="$PACKAGE_SECTION_PATH\$PACKAGE_VERSION"
	$PACKAGE_BIN_PATH="$PACKAGE_RES_PATH\bin"

	New-Item -ItemType Directory -Force -Path "$TMP_DIR" | out-null

	if (!(Test-Path $PACKAGE_RES_PATH)) {
		if (!(Test-Path $PACKAGE_DL_PATH)) {
			Write-Host "Downloading $PACKAGE_NAME from '$PACKAGE_URL'..."
			Invoke-WebRequest $PACKAGE_URL -OutFile $PACKAGE_DL_PATH
			if(!($?)) {
				Write-Host "[Failed] Download $PACKAGE_NAME"
				return $null
			}
		}

		$PACKAGE_UNZIPPED=(Get-ChildItem "$TMP_DIR" -Filter $PACKAGE_NAME-$PACKAGE_VERSION* -Name)

		if ($PACKAGE_UNZIPPED) {
			$PACKAGE_TMP_PATH=-join("$TMP_DIR\", "$PACKAGE_UNZIPPED")
		} else {
			Write-Host "Unzip '$PACKAGE_DL_PATH'..."
			Expand-Archive "$PACKAGE_DL_PATH" -DestinationPath "$TMP_DIR"
			$PACKAGE_TMP_PATH=-join("$TMP_DIR\", (Get-ChildItem "$TMP_DIR" -Filter $PACKAGE_NAME-$PACKAGE_VERSION* -Name))
		}

		Write-Host "Move '$PACKAGE_TMP_PATH' to '$PACKAGE_RES_PATH'"
		New-Item -ItemType Directory -Force -Path "$PACKAGE_SECTION_PATH" | out-null
		mv -Force "$PACKAGE_TMP_PATH" "$PACKAGE_RES_PATH"
	}

	Write-Host "Installed '$PACKAGE_NAME-$PACKAGE_VERSION' at '$PACKAGE_RES_PATH'"

	$global:ENV_PATH_STR=-join($global:ENV_PATH_STR, ";$PACKAGE_BIN_PATH")
}

# ============================ Setup ninja-build ===============================
function setup_ninja_build()
{
	$PACKAGE_NAME="ninja"
	$PACKAGE_VERSION=$NinjaVersion
	$PACKAGE_URL="https://github.com/ninja-build/ninja/releases/download/v$PACKAGE_VERSION/ninja-win.zip"
	$PACKAGE_DL_PATH="$DOWNLOAD_DIR\$PACKAGE_NAME-$PACKAGE_VERSION.zip"
	$PACKAGE_SECTION_PATH="$RES_DIR\$PACKAGE_NAME"
	$PACKAGE_RES_PATH="$PACKAGE_SECTION_PATH\$PACKAGE_VERSION"
	$PACKAGE_BIN_PATH="$PACKAGE_RES_PATH"

	New-Item -ItemType Directory -Force -Path "$TMP_DIR" | out-null

	if (!(Test-Path $PACKAGE_RES_PATH)) {
		if (!(Test-Path $PACKAGE_DL_PATH)) {
			Write-Host "Downloading $PACKAGE_NAME from '$PACKAGE_URL'..."
			Invoke-WebRequest $PACKAGE_URL -OutFile $PACKAGE_DL_PATH
			if(!($?)) {
				Write-Host "[Failed] Download $PACKAGE_NAME"
				return $null
			}
		}

		$PACKAGE_UNZIPPED=(Get-ChildItem "$TMP_DIR" -Filter $PACKAGE_NAME-$PACKAGE_VERSION* -Name)

		if ($PACKAGE_UNZIPPED) {
			$PACKAGE_TMP_PATH=-join("$TMP_DIR\", "$PACKAGE_UNZIPPED")
		} else {
			Write-Host "Unzip '$PACKAGE_DL_PATH'..."
			Expand-Archive "$PACKAGE_DL_PATH" -DestinationPath "$TMP_DIR"
			$PACKAGE_TMP_PATH=-join("$TMP_DIR\", (Get-ChildItem "$TMP_DIR" -Filter $PACKAGE_NAME-$PACKAGE_VERSION* -Name))
		}

		Write-Host "Move '$PACKAGE_TMP_PATH' to '$PACKAGE_RES_PATH'"
		New-Item -ItemType Directory -Force -Path "$PACKAGE_SECTION_PATH" | out-null
		mv -Force "$PACKAGE_TMP_PATH" "$PACKAGE_RES_PATH"
	}

	Write-Host "Installed '$PACKAGE_NAME-$PACKAGE_VERSION' at '$PACKAGE_RES_PATH'"

	$global:ENV_PATH_STR=-join($global:ENV_PATH_STR, ";$PACKAGE_BIN_PATH")
}

# ======================== FreeRTOS-FAT git init ===================================
function git_freertos_fat() {
	$GIT_BRANCH="master"
	$GIT_TARGET_PATH="$RES_DIR\FreeRTOS-FAT-CLI\$GIT_BRANCH"
	$GIT_URL="https://github.com/carlk3/FreeRTOS-FAT-CLI-for-RPi-Pico.git"

	if (!(Test-Path "$GIT_TARGET_PATH")) {
		New-Item -ItemType Directory -Force -Path "$GIT_TARGET_PATH" | out-null
		git clone --branch=$GIT_BRANCH --recurse-submodules "$GIT_URL" "$GIT_TARGET_PATH"
	}

	[Environment]::SetEnvironmentVariable(
		"FREERTOS_FAT_PICO_PATH",
		$GIT_TARGET_PATH,
		[System.EnvironmentVariableTarget]::User
	)

	Write-Host "FreeRTOS-FAT-CLI-for-RPi-Pico is installed"
}

# ======================== FreeRTOS git init ===================================
function git_freertos() {
	$GIT_BRANCH=$FreeRTOSVersion
	$GIT_TARGET_PATH="$RES_DIR\FreeRTOS-LTS\$GIT_BRANCH"
	$GIT_URL="https://github.com/FreeRTOS/FreeRTOS-LTS.git"
	$GIT_KERNEL_PATH="$GIT_TARGET_PATH\FreeRTOS\FreeRTOS-Kernel"

	if (!(Test-Path "$GIT_TARGET_PATH")) {
		New-Item -ItemType Directory -Force -Path "$GIT_TARGET_PATH" | out-null
		git clone --depth=1 --branch=$GIT_BRANCH --recurse-submodules "$GIT_URL" "$GIT_TARGET_PATH"
	}

	[Environment]::SetEnvironmentVariable(
		"FREERTOS_LTS_PATH",
		$GIT_TARGET_PATH,
		[System.EnvironmentVariableTarget]::User
	)

	[Environment]::SetEnvironmentVariable(
		"FREERTOS_KERNEL_PATH",
		$GIT_KERNEL_PATH,
		[System.EnvironmentVariableTarget]::User
	)

	Write-Host "FreeRTOS $GIT_BRANCH is installed"

	git_freertos_fat
}

# ======================== pico-sdk git init ===================================
function git_pico_sdk() {
	$GIT_BRANCH=$PicoSDKVersion
	$GIT_TARGET_PATH="$RES_DIR/pico-sdk/$GIT_BRANCH"
	$GIT_URL="https://github.com/raspberrypi/pico-sdk.git"

	if (!(Test-Path "$GIT_TARGET_PATH")) {
		New-Item -ItemType Directory -Force -Path "$GIT_TARGET_PATH" | out-null
		git clone --depth=1 --branch=$GIT_BRANCH --recurse-submodules "$GIT_URL" "$GIT_TARGET_PATH"
	}

	Write-Host "Pico-SDK $GIT_BRANCH is installed"

	[Environment]::SetEnvironmentVariable(
		"PICO_SDK_PATH",
		$GIT_TARGET_PATH,
		[System.EnvironmentVariableTarget]::User
	)
}

# Compiling pico-sdk project required to be inside Dev command prompt
$isRunningDevCmd = !!(Get-Command 'tf' -ErrorAction SilentlyContinue)
if (!($isRunningDevCmd)) {
	Write-Host "It appears you're not in VS Developer Command Prompt."
	Write-Host "Switch to VS Developer Command Prompt or install Visual Studio first if it's not installed yet."
	Write-Host "Exiting..."

	Exit 1
}

if ($ToolchainInstall) { setup_arm_gnu_toolchain }
if ($NinjaInstall) { setup_ninja_build }
if ($OpenocdInstall) { setup_openocd }
if ($CmakeInstall) { setup_cmake }
if ($FreeRTOSInstall) { git_freertos }
if ($PicoSDKInstall) { git_pico_sdk }

# check if environment variable is already appended
$already=Select-String -Path $profile.CurrentUserCurrentHost -Pattern "$SetupTitle"

# set environment variable
if ($already -eq $null) {
	$global:SET_ENV_PATH_STR=-join(
		"`n", "# === $SetupTitle v$SetupVersion - Start ===",
		"`n", "# - arm-gnu-toolchain $ToolchainVersion",
		"`n", "# - CMake $CmakeVersion",
		"`n", "# - OpenOcd $OpenocdVersion",
		"`n", "# - Ninja $NinjaVersion",
		"`n", "# - FreeRTOS-LTS $FreeRTOSVersion",
		"`n", "# - pico-sdk $PicoSDKVersion",
		"`n", '$env:Path += "', "$global:ENV_PATH_STR", '"',
		"`n", '# === $SetupTitle v$SetupVersion - End ===',
		"`n"
	)
	Write-Host "---------------------------------"
	Write-Host "$global:SET_ENV_PATH_STR"
	Add-Content -Path $profile.CurrentUserCurrentHost -Value $global:SET_ENV_PATH_STR
} else {
	Write-Host "---------------------------------"
	Write-Host "$SetupTitle paths is already in environment."
}
