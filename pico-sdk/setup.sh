#!/bin/bash

set -e

DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"

SETUP_TITLE="PICO-SDK SETUP",
SETUP_VERSION="0.2.0"

BASHRC_PATH="${HOME}/.bashrc"

MAIN_PATH="${DIR}/pico"
mkdir -p "${MAIN_PATH}"

if grep -q "${SETUP_TITLE}" "${BASHRC_PATH}"; then
	echo "Previous Installation found at ${BASHRC_PATH}"
	echo "Please corresponding lines at '${BASHRC_PATH}' and delete all files in '${MAIN_PATH}'"
	exit 1
fi

# install required packages
sudo apt-get update
sudo apt-get install -y gcc-arm-none-eabi \
	libnewlib-arm-none-eabi \
	libstdc++-arm-none-eabi-newlib \
	build-essential \
	ninja-build \
	cmake \
	openocd \
	gdb-multiarch \
	git

# ================== FreeRTOS Kernel init ======================================
FREERTOS_KERNEL_PATH="${MAIN_PATH}/FreeRTOS-Kernel"
FREERTOS_KERNEL_BRANCH="main"
FREERTOS_KERNEL_URL="https://github.com/raspberrypi/FreeRTOS-Kernel.git"
git clone --branch=$FREERTOS_KERNEL_BRANCH --recurse-submodules --depth=1 \
	"${FREERTOS_KERNEL_URL}" "${FREERTOS_KERNEL_PATH}"

echo "FINISHED - Cloned ${FREERTOS_KERNEL_URL}"

# ============== FreeRTOS-FAT-CLI-for-RPi-Pico init ============================
FREERTOS_FAT_PICO_PATH="${MAIN_PATH}/FreeRTOS-FAT-PICO"
FREERTOS_FAT_PICO_BRANCH="master"
FREERTOS_FAT_PICO_URL="https://github.com/carlk3/FreeRTOS-FAT-CLI-for-RPi-Pico.git"
git clone --branch=$FREERTOS_FAT_PICO_BRANCH --recurse-submodules --depth=1 \
	"${FREERTOS_FAT_PICO_URL}" "${FREERTOS_FAT_PICO_PATH}"

echo "FINISHED - Cloned ${FREERTOS_FAT_PICO_URL}"

# ================== FreeRTOS-LTS init =========================================
FREERTOS_LTS_PATH="${MAIN_PATH}/FreeRTOS-LTS"
FREERTOS_LTS_BRANCH="202406-LTS"
FREERTOS_LTS_URL="https://github.com/FreeRTOS/FreeRTOS-LTS.git"
git clone --branch=$FREERTOS_LTS_BRANCH --recurse-submodules --depth=1 \
	"${FREERTOS_LTS_URL}" "${FREERTOS_LTS_PATH}"

echo "FINISHED - Cloned ${FREERTOS_LTS_URL}"

# ================== pico-sdk init =============================================
PICO_SDK_PATH="${MAIN_PATH}/sdk"
PICO_SDK_URL="https://github.com/raspberrypi/pico-sdk.git"

# use 1.5.1 as the default pico-sdk version
if [ -z "${PICO_SDK_VERSION}" ]; then
	PICO_SDK_VERSION="2.0.0"
fi

git clone --branch="${PICO_SDK_VERSION}" --recurse-submodules --depth=1 \
	"${PICO_SDK_URL}" "${PICO_SDK_PATH}"

echo "FINISHED - Cloned ${PICO_SDK_URL}"

# ================== pico-examples init ========================================
PICO_EXAMPLES_PATH="${MAIN_PATH}/examples"
PICO_EXAMPLES_URL="https://github.com/raspberrypi/pico-examples.git"
PICO_EXAMPLES_VERSION="sdk-${PICO_SDK_VERSION}"
git clone --branch="${PICO_EXAMPLES_VERSION}" --recurse-submodules --depth=1 \
	"${PICO_EXAMPLES_URL}" "${PICO_EXAMPLES_PATH}"

echo "FINISHED - Cloned ${PICO_EXAMPLES_URL}"

# ================ Set Environment Variable in bashrc ==========================
printf "\n# === ${SETUP_TITLE} v${SETUP_VERSION} - START [$(date +%F\ %T\ %Z)] ===\n" \
	| tee -a "${BASHRC_PATH}" > /dev/null

# Use ninja as the default build, instead of make
printf "export CMAKE_GENERATOR=Ninja\n\n" \
	| tee -a "${BASHRC_PATH}" > /dev/null
printf "export PICO_SDK_PATH=\"%s\"\n" "${PICO_SDK_PATH}"\
	| tee -a "${BASHRC_PATH}" > /dev/null
printf "export FREERTOS_LTS_PATH=\"%s\"\n" "${FREERTOS_LTS_PATH}"\
	| tee -a "${BASHRC_PATH}" > /dev/null
printf "export FREERTOS_KERNEL_PATH=\"%s\"\n" "${FREERTOS_KERNEL_PATH}"\
	| tee -a "${BASHRC_PATH}" > /dev/null
printf "export FREERTOS_FAT_PICO_PATH=\"%s\"\n" "${FREERTOS_FAT_PICO_PATH}"\
	| tee -a "${BASHRC_PATH}" > /dev/null

printf "\n# === ${SETUP_TITLE} v${SETUP_VERSION} - END   [$(date +%F\ %T\ %Z)] ===\n" \
	| tee -a "${BASHRC_PATH}" > /dev/null

echo "FINISHED - ALL"
