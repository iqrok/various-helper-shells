#!/bin/sh

ARPSCAN="$(command -v arp-scan)"
IPCMD="$(command -v ip)"
DELAY_SEC=0

# ============================== IP4 Utilities =================================
PAT_IP_OCTET='(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)'
PAT_IP_MASK='(3[0-2](\$|\s)|[1-2][0-9](\$|\s)|[0-9](\$|\s)|[0][0-9]+)'
PAT_IP_ADDR="${PAT_IP_OCTET}(\.${PAT_IP_OCTET}){3}"
PAT_IP_CIDR="${PAT_IP_ADDR}\/${PAT_IP_MASK}"

normalize_ip_str() {
	CIDR_STR="${1}"
	printf "%s" "${CIDR_STR}" \
		| sed -Ee 's/^[0]*([1-9]*[0-9].*$)/\1/g' \
		| sed -Ee 's/(\.)[0]*([1-9]*[0-9])/\1\2/g' \
		| sed -Ee 's/(.*?)\/[0]*([1-9]*[0-9])/\1\/\2/g'
}

extract_cidr() {
	INPUT_STR="${1}"
	CIDR_STR="$(printf "${INPUT_STR}" | grep -Po "${PAT_IP_CIDR}")"

	test -z "${CIDR_STR}" && return 2

	normalize_ip_str "${CIDR_STR}"
	return 0
}

extract_ip_address() {
	INPUT_STR="${1}"
	IP_ADDR_STR="$(printf "%s" "${INPUT_STR}" | grep -Po  "${PAT_IP_ADDR}")"

	test -z "${IP_ADDR_STR}" && return 2

	normalize_ip_str "${IP_ADDR_STR}"
	return 0
}

get_iface_from_ip() {
	INPUT_IP="${1}"

	# Notes:
	# ip r show to match <ip range>
	#	selects routes with prefixes not longer than <ip range>
	#
	# grep -v 'default'
	#	omit default route, otherwise the default one will always be selected
	#
	# head -n1
	#	select route with highest metric value
	#
	# sed -E -e 's,.*dev\s*?([^[:space:]\/]*?)\s.*$,\1,g'
	#	get iface name. Iface name shouldn't contains whitspace nor '/'
	#	https://git.kernel.org/pub/scm/network/iproute2/iproute2.git/tree/lib/utils.c?id=1f420318bda3cc62156e89e1b56d60cc744b48ad#n827
	IFACE_DEV="$("${IPCMD}" route show to match "${INPUT_IP}" \
		| grep -v 'default' \
		| head -n1 \
		| sed -E -e 's,.*dev\s*?([^[:space:]\/]*?)\s.*$,\1,g')"

	if [ -z "${IFACE_DEV}" ]; then
		echo "Can't find Network interface for this ip range '${INPUT_IP}'"
		return 2
	fi

	echo "${IFACE_DEV}"
	return 0
}
# ============================== === ========= =================================

show_help() {
echo "$(basename "$0") [OPTIONS] <IP RANGE> -- scan available IP addresses at given IP range

OPTIONS:
  <IP RANGE>            IP addresses range in CIDR notation (i.e. 192.168.1.1/24)
  -h, --help            show this help
  -I, -i, --iface=NAME  set specific network interface name
  -s, --sort            sort scanned IP addresses in ascending order
  -n, --delay=N         if N > 0, the scan will be repeated N seconds after the last scan finished.
                        Otherwise, single time scan will be performed.

EXAMPLES:
  $(basename "$0") 10.10.0.1/22
  $(basename "$0") -I eth0
  $(basename "$0") -I eth0 10.10.0.1/24 -n10
"
}

scan_range() {
	SCAN_IP="${1}"
	SCAN_IFACE="${2}"
	SCAN_SORT="${3}"
	COMMAND="${ARPSCAN} -qxI ${SCAN_IFACE} ${SCAN_IP}"

	# add 'sudo' if user is not root
	test "$(id -u)" != "0" && COMMAND="sudo ${COMMAND}"

	printf "Scanning \033[1m%s\033[0m from \033[1m%s\033[0m" \
		"${IP_RANGE}" "${IFACE}"

	printf "\n==========================================\n"

	T_START="$(date +%s)"
	if [ -z "${SCAN_SORT}" ]; then
		${COMMAND} 2> /dev/null
	else
		${COMMAND} 2> /dev/null | sort -n -t. -k1,1 -k2,2 -k3,3 -k4,4
	fi
	T_END="$(date +%s)"

	printf "==========================================\nScanning took %ds\n" \
		$(( T_END - T_START ))
}

# ============================= Parsing Options ================================
if ! ARGS="$(getopt -n "$(basename "$0")" \
		-o "hI:i:n:s" \
		-l "help,iface:,delay:,sort" \
		-- "$@")"
then
	echo "Error: Failed getopt execution!"
	exit 1
fi

eval set -- "${ARGS}"

while :; do
	case "${1}" in
		-h|--help)
			show_help
			exit 0
			;;

		-I|-i|--iface)
			IFACE="${2}"
			shift 2
			;;

		-s|--sort)
			SORT_RESULT="1"
			shift
			;;

		-n|--delay)
			DELAY_SEC="${2}"
			if ! test "${DELAY_SEC}" -gt "0"; then
				echo "WARNING: Delay must be a number and greater than 0. delay won't be used!"
				DELAY_SEC=0
			fi
			shift 2
			;;

		# end of option arguments
		--)
			shift
			break
			;;

		*)
			;;

	esac
done
# ============================= =============== ================================

PARAM_IP="${1}"

# get CIDR from parameter
IP_CIDR="$(extract_cidr "${PARAM_IP}")"

# throw error as nothing is provided
if [ -z "${IP_CIDR}" ] && [ -z "${IFACE}" ] ; then
	test -n "${PARAM_IP}" \
		&& printf "IP address '${PARAM_IP}' is invalid!\n"

	printf "Error: Both Network Interface and IP range are empty!\n\n"
	show_help
	exit 1
fi

# ip range is not provided, use interface ip range
if [ -z "${IP_CIDR}" ]; then
	set -x
	if ! IFACE_DETAILS="$("${IPCMD}" -br address show dev "${IFACE}")"; then
		exit 1
	fi

	if ! IP_CIDR="$(extract_cidr "${IFACE_DETAILS}")"; then
		echo "Error: Can't find IP for interface '${IFACE}'!"
		exit 1
	fi
fi

# network interface is not provided, use find interface from the ip range
if [ -z "${IFACE}" ]; then
	IP_ADDR="$(extract_ip_address "${IP_CIDR}")"
	if ! IFACE="$(get_iface_from_ip "${IP_ADDR}")"; then
		echo "Error: Can't find interface for '${IP_CIDR}'!"
		exit 1
	fi
fi

# ===== main execution - single run =====
if ! test "${DELAY_SEC}" -gt "0"; then
	scan_range "${IP_CIDR}" "${IFACE}" "${SORT_RESULT}"
	exit "${?}"
fi

# ===== main execution - until cancelled by input ====

# enable job control
set -m

# hide ^C
stty -echoctl

# infinite loop flags
IS_RUNNING=1

# callback trap
ctrl_c() {
	IS_RUNNING=0
}

# trap sigint
trap ctrl_c INT

# infinite loop
while test "${IS_RUNNING}" = "1"; do
	clear

	scan_range "${IP_CIDR}" "${IFACE}" "${SORT_RESULT}"

	T_START=$(date +%s)
	T_WAIT="${DELAY_SEC}"
	while :; do
		test "${IS_RUNNING}" != "1" && break

		T_WAIT=$(( DELAY_SEC - ($(date +%s) - T_START) ))

		printf "Press \033[1mCtrl+c\033[0m to stop\t| \033[1m%3d\033[0ms\r" \
			"${T_WAIT}"

		test "${T_WAIT}" -le "0" && break

		sleep 1
	done
done
