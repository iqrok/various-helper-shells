#!/bin/sh

if [ "$(id -u)" != "0" ]; then
	echo "Must be run as root!"
	exit 1
fi

USER_ACTUAL="$(id -un "${SUDO_UID}")"

install() {
	ARPSCAN="$(command -v arp-scan)"

	if [ -z "${ARPSCAN}" ]; then
		ech "Installing 'arp-scan'..."
		apt install arp-scan -y
		ARPSCAN="$(command -v arp-scan)"
	fi

	if ! getcap "${ARPSCAN}" | grep -q cap_net_raw; then
		echo "Setting CAP_NET_RAW capabilities to '${ARPSCAN}'..."
		setcap cap_net_raw=p "${ARPSCAN}"
	fi

	DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"
	TARGET_DIR="/usr/local/bin"

	NAME_EXE="ipscan"
	SRC_FILE="${DIR}/${NAME_EXE}"
	DST_FILE="${TARGET_DIR}/${NAME_EXE}"

	echo "Installing 'ipscan' at '${DST_FILE}'..."
	cat "${SRC_FILE}" \
		| tee "${DST_FILE}" > /dev/null \
		&& chmod a+x "${DST_FILE}"

	SUDOER_FILE='/etc/sudoers.d/ipscan'
	echo "Setting nopasswd for '${USER_ACTUAL}' to execute '${ARPSCAN}'..."
	echo "${USER_ACTUAL} ALL=NOPASSWD: ${ARPSCAN}" \
		| tee "${SUDOER_FILE}" > /dev/null

	echo "Finished."

	exit 0
}

uninstall() {
	ARPSCAN="$(command -v arp-scan)"
	IPSCAN_PATH="$(command -v ipscan)"

	if [ -z "${IPSCAN_PATH}" ]; then
		echo "'ipscan' is not installed! Exiting..."
		exit 1
	fi

	echo "Removing 'ipscan' at '${IPSCAN_PATH}'..."
	rm "${IPSCAN_PATH}"

	if getcap "${ARPSCAN}" | grep -q cap_net_raw; then
		echo "Removing CAP_NET_RAW capabilities to '${ARPSCAN}'..."
		setcap -r "${ARPSCAN}"
	fi

	SUDOER_FILE='/etc/sudoers.d/ipscan'
	if [ -f "${SUDOER_FILE}" ]; then
		echo "Removing nopasswd for '${USER_ACTUAL}' to execute '${ARPSCAN}'..."
		rm "${SUDOER_FILE}"
	fi

	echo "'ipscan' is removed. 'arp-scan' is not removed automatically."

	exit 0
}

#------------------ MAIN ------------------
IS_INSTALLING=1
while getopts ":u" OPT; do
	case "${OPT}" in
		u)
			IS_INSTALLING=0
			;;

		?)
			echo "Invalid option: -${OPT}"
			;;
	esac
done

case "${IS_INSTALLING}" in
	1)
		install
		;;

	0)
		uninstall
		;;
esac
