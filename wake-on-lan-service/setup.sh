#!/bin/sh

set -e

if [ "$(id -u)" != "0" ]; then
	echo "Please run this script as root!"
	exit 1
fi

CURRENT_DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"

SERVICE_TMPL_FILE="${CURRENT_DIR}/wol-enable.template"
SERVICE_TMPL="$(cat "${SERVICE_TMPL_FILE}")"
SERVICE_DIR="/etc/systemd/system"
ETHTOOL_BIN="$(command -v ethtool)"

if [ -z "${ETHTOOL_BIN}" ]; then
	echo "Installing 'ethtool'..."
	apt-get update
	apt-get install -y ethtool
fi

array_index() {
	shift "${1}" # Shift N number of elements, including the first argument

	# Return non-zero if index is out of bounds ($1 will be empty)
	echo "${1:?Index out of bounds}" # Print the first item after shifting
}

iface_list() {
	ip -br a | grep -P '^en' | awk '{print $1}'
}

iface_array() {
	iface_list | tr '\n' ' '
}

iface_menu() {
	COUNTER=0

	printf "List of available Network Interface(s)\n"
	echo "=========================="
	printf "INDEX\t| NAME\n"
	echo "--------+-----------------"
	printf "%2d\t| %s\n" "${COUNTER}" "ALL"

	iface_list | while read -r IFACE_NAME; do
		COUNTER=$(( COUNTER + 1 ))
		printf "%2d\t| %s\n" "${COUNTER}" "${IFACE_NAME}"
	done
	echo "=========================="
}

generate_wol_service() {
	TARGET_IFACE="${1}"
	SERVICE_NAME="wol-enable-${TARGET_IFACE}.service"
	TARGET_PATH="${SERVICE_DIR}/${SERVICE_NAME}"

	echo "- Wol Service for ${TARGET_IFACE}"
	echo "  - Generate WoL service for iface '${TARGET_IFACE}'"
	echo "${SERVICE_TMPL}" \
		| sed "s,__ETHTOOL_BIN__,${ETHTOOL_BIN},g" \
		| sed "s,__TARGET_IFACE__,${TARGET_IFACE},g" \
		| tee "${TARGET_PATH}" > /dev/null

	echo "  - Reload systemctl"
	systemctl daemon-reload

	echo "  - Enable '${SERVICE_NAME}'"
	systemctl enable "${SERVICE_NAME}"
	IS_ENABLED="$(systemctl is-enabled "${SERVICE_NAME}")"

	echo "  - ${SERVICE_NAME} enabled status '${IS_ENABLED}'"

	echo "  - Start ${SERVICE_NAME}"
	systemctl start "${SERVICE_NAME}"

	if ethtool "${TARGET_IFACE}" | grep -q 'Wake-on: g'; then
		echo "  - WoL ${TARGET_IFACE} is Enabled"
	else
		echo "  - Failed to Enable WoL for '${TARGET_IFACE}'"
	fi
	echo "===================================================================="
}

iface_main() {
	# show menu
	iface_menu

	# ask which interface to set
	printf "Select Target Interface INDEX: "
	read -r SELECTED_IFACE

	if [ "${SELECTED_IFACE}" != "0" ]; then
		# intentionally not quoting iface_array
		IFACE_NAME="$(array_index "${SELECTED_IFACE}" $(iface_array))"
		generate_wol_service "${IFACE_NAME}"
	else
		iface_list | while read -r IFACE_NAME; do
			generate_wol_service "${IFACE_NAME}"
		done
	fi

	echo "- Finished"
}

# =========================== MAIN =============================================
iface_main
