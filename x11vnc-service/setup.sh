#!/bin/sh

if [ "$(id -u)" = "0" ]; then
	echo "Don't run as root!"
	exit 1
fi

compare_apt_version()
{
	MIN_VERSION="$(echo "${1}" | tr \. '0')"
	APT_VERSION="$(apt-cache policy tigervnc-common | grep "Candidate" \
		| grep -Po '[0-9]+\.[0-9]+\.[0-9]+' | tr \. '0')"

	if [ "${APT_VERSION}" -ge "${MIN_VERSION}" ]; then
		return 0
	fi

	return 1
}

get_arch()
{
	O_UNAME_M="$(uname -m)"
	case "${O_UNAME_M}" in
		"aarch64"|"arm64")
			printf "arm64"
			;;

		"x86_64"|"amd64")
			printf "amd64"
			;;

		*)
			;;

	esac
}

install_tigervnc()
{
	# needs min tigervnc v1.13.0
	TIGERVNC_VERSION="1.13.1"
	TIGERVNC_UBUNTU="22.04LTS"
	TIGERVNC_ARCH="$(get_arch)"
	TIGERVNC_DEB_FILENAME="tigervncserver_${TIGERVNC_VERSION}-1ubuntu1_${TIGERVNC_ARCH}.deb"
	TIGERVNC_OUTPUT_DEB="/tmp/${TIGERVNC_DEB_FILENAME}"
	TIGERVNC_SF_URL="https://jaist.dl.sourceforge.net/project/tigervnc/stable/${TIGERVNC_VERSION}/ubuntu-${TIGERVNC_UBUNTU}/${TIGERVNC_ARCH}/${TIGERVNC_DEB_FILENAME}"

	if compare_apt_version "${TIGERVNC_VERSION}"; then
		sudo apt install -y tigervnc-scraping-server
		return ${?}
	fi

	if [ ! -f "${TIGERVNC_OUTPUT_DEB}" ]; then
		echo "Downloading ${TIGERVNC_OUTPUT_DEB}..."
		wget "${TIGERVNC_SF_URL}" -O "${TIGERVNC_OUTPUT_DEB}"
	fi

	sudo apt install -y "${TIGERVNC_OUTPUT_DEB}"
}

# User setup
TARGET_USER="$(logname)"
TARGET_HOME="$(grep "${TARGET_USER}" /etc/passwd | awk '{ print $6 }' FS=:)"

if [ "$(id -u "${TARGET_USER}")" = "0" ]; then
	echo "'root' can't be set as the VNC user!"
	exit 2
fi

# Check Required dependencies
BIN_OPENSSL="$(command -v openssl)"
BIN_TIGERVNC="$(command -v x0vncserver)"

if [ -z "${BIN_TIGERVNC}" ]; then
	echo "Installing 'TigerVNC'...."
	install_tigervnc
	BIN_TIGERVNC="$(command -v x0vncserver)"
fi

if [ -z "${BIN_OPENSSL}" ]; then
	echo "Installing 'openssl'...."
	sudo apt install -y openssl
	BIN_OPENSSL="$(command -v openssl)"
fi

DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"

SERVICE_NAME="x11vncd"
SRC_SERVICE_FILE="${DIR}/${SERVICE_NAME}.service"
DST_SYTEMD_DIR="${TARGET_HOME}/.config/systemd/user"

if [ ! -d "${DST_SYTEMD_DIR}" ]; then
	mkdir -p "${DST_SYTEMD_DIR}"
fi

DST_VNC_CONFIG_DIR="${TARGET_HOME}/.vnc"
DST_VNC_RSAKEY_FILE="${DST_VNC_CONFIG_DIR}/vnckey.pem"
DST_VNC_PASSWD_FILE="${DST_VNC_CONFIG_DIR}/passwd"
DST_SERVICE_FILE="${DST_SYTEMD_DIR}/${SERVICE_NAME}.service"

if [ ! -d "${DST_VNC_CONFIG_DIR}" ]; then
	mkdir "${DST_VNC_CONFIG_DIR}"
fi

# create vnc password
while ! vncpasswd; do
	echo "Failed to set VNC Password! Try Again"
done

# create RSA key 4096 bit
"$BIN_OPENSSL" genrsa -out "${DST_VNC_RSAKEY_FILE}" 4096

# installing systemd service
sed "s,__TARGET_HOME__,${TARGET_HOME},g" "${SRC_SERVICE_FILE}" \
	| sed "s,__DST_VNC_RSAKEY_FILE__,${DST_VNC_RSAKEY_FILE}," \
	| sed "s,__DST_VNC_PASSWD_FILE__,${DST_VNC_PASSWD_FILE}," \
	| tee "${DST_SERVICE_FILE}" > /dev/null

systemctl --user daemon-reload
systemctl --user enable "${SERVICE_NAME}.service"
systemctl --user start "${SERVICE_NAME}.service"
sleep 1
systemctl --user status "${SERVICE_NAME}.service"
