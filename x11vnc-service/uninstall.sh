#!/bin/sh

DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"

# User setup
TARGET_USER="$(logname)"
TARGET_HOME="$(grep "${TARGET_USER}" /etc/passwd | awk '{ print $6 }' FS=:)"

SERVICE_NAME="x11vncd"
DST_SYTEMD_DIR="${TARGET_HOME}/.config/systemd/user"

DST_VNC_CONFIG_DIR="${TARGET_HOME}/.vnc"
DST_VNC_RSAKEY_FILE="${DST_VNC_CONFIG_DIR}/vnckey.pem"
DST_VNC_PASSWD_FILE="${DST_VNC_CONFIG_DIR}/passwd"
DST_SERVICE_FILE="${DST_SYTEMD_DIR}/${SERVICE_NAME}.service"

# remove service
systemctl --user stop "${SERVICE_NAME}.service"
systemctl --user disable "${SERVICE_NAME}.service"

rm -f "${DST_SERVICE_FILE}"

systemctl --user daemon-reload

# cleanup vnc credentials
rm -rf "${DST_VNC_CONFIG_DIR}"

# remove tigervnc server
sudo apt remove --purge -y *tigervnc* \
	&& sudo apt autoremove --purge -y
